''' @file    Lab 0x02.py 
    @brief   Documentation for use of Lab 0x02.py. Reaction Test. Think Fast!
    @details This code randomly lights an LED on a STM32L476 Nucleo for one 
             second. The user then must press the blue push button before the 
             LED truns off. When the user starts the program the LED will 
             light up randomly every 2 to 3 seconds. The user will then have 
             one second to press the blue button on the nucleo. The code will
             take the time between the LED lighting up and the button press 
             and store that value. Once the user quits the program the average
             reaction time is recorded. 
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    April. 26, 2020
'''

## import micropython
## micropython.alloc_emergency_exception_buf(100)

import utime
import random
import pyb
pyb.enable_irq    # enables interupt servie routines

global compare    # make this compare variable global

# Initialize timers and pins 
pinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)      # initialize LED Pin
pinPC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)    # initialize blue button pin
tim2 = pyb.Timer(2, prescaler = 656, period = 0x7FFFFFFF)  # initialize timer with prescaler
tim5 = pyb.Timer(5, prescaler = 656, period = 0x7FFFFFFF)  # initialize timer with prescaler

start_time = utime.ticks_us()

# Create a time list and inerval for comparing
channel = [0]
compare = 0

def OC_INTERRUPT(line):
    '''
    @brief This function call when the pushbutton is pressed and records the time accordingly
    @param line   A param that allows the interupt to occur mulitple times
    '''
    # interrupt occurs under following condition
    if channel[compare] == 0:              # if the LED has been lit and replaced prepare to record value
        channel[compare] = tim2.counter()  # record the reaction time
        
    else:
        print('Button not accepted. Please wait fot LED to light.')
        
# Initialize the Interrupt
extint = pyb.ExtInt(pinPC13, pyb.ExtInt.IRQ_FALLING, pyb.PIN.PULL_UP, callback=pushed)


# Main Loop that holds the code
while True:
    try: 
        if len(channel)>compare:
            # main loop thats is executed each iteration until ctrl+C is detected
            light_on = random.randint(1,1000)     # Generate random integer to turn LED on
            delay = 2 + light_on/1000             # Apply the random integer to LED timing delay
            tim5.counter(0)                       # Stop triggering of delay if LED not lit
            utime.sleep(delay)                    # Pause timer during delay when light is not on
            pinA5.high()                          # Turn on LED
            tim2.counter(0)                       # Start the timer for calculations
            utime.sleep(1)                        # Leave LED on for 1 second under all conditions
            pinA5.low()                           # Turn off LED
            compare = len(channel)
            
        # If the channel list length gets filled increase the list length
        elif channel[len(channel)-1] != 0:
            channel.append(0)
            
    except KeyboardInterrupt:
        # handles the exit condition
        # must calculate average reaction time here
        print(channel)
        ave_time = sum(channel)/(len(channel)-1)
        print('The average reactiong time is: ' + str(ave_time) + 'microseconds.')
        quit()
        
        
        