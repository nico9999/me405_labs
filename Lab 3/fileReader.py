# This example will help with the PC front end of Lab 0x03
#
# All code in this example will run on the PC

import array
import matplotlib
from matplotlib import pyplot # will import just the pyplot class from the
                              # matplotlib module

## An array to hold time data as floating point numbers
time = array.array('f')

## An array to hold output data as floating point numbers
data = array.array('f')

with open('dataFile.csv','r') as file:
    # everything inside the "with" block will be able to read from "file"
    # but after un-indenting the "with" will automatically close the file.
    # You will use "with serial('COM3', baudrate=115200) as ser:" or similar
    
    # iterate through each line in the file
    for line in file:
        ## First strip line endings ("\r" or "\n")
        ## This is handled automatically for the file, but 
        # will need to be done manually with the serial data
        print("Line=",line)
        print("Stripped Line=",line.strip())
        # Split each string into two strings on the commas
        print("Split Line=",line.strip().split(','))
        [t, y] = line.strip().split(',')
        print("t=",t)
        print("y=",y)
        # Convert strings to floats
        time.append(float(t))
        data.append(float(y))
        print('')

pyplot.figure()
pyplot.plot(time,data)
pyplot.xlabel('Time, t')
pyplot.ylim([-1, 1])
pyplot.yticks([-1, -0.5, 0, 0.5, 1])
pyplot.ylabel('Output, y')
pyplot.title('y vs t')