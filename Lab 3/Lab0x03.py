'''
@file main.py
@brief This file handles the nucleo side of Lab 3, recording ADC voltages.
@details This file waits the UI to send the code to begin data collection, and
         then waits for the user to press the blue button. Once the button has
         been pressed, the code stores the Voltage data in buffy and the the 
         time data in time_data. It then sends these arrays to the UI for 
         proccessing and plotting. 
@author Nicholas Holman
@author Vikram Thridandum 
@date May 7th, 2021
'''

## Import necessary modules
from pyb import UART
import array
import pyb
pyb.repl_uart(None)
pyb.enable_irq   #Allows for interrupt

## Variables for Running ADC Calcs
freq = 200000
ranger = 1000
char = 'g'
## UART Initialization
uart = UART(2)

## Buffer Array for ADC
buffy = array.array('H', (0 for index in range(ranger)))

# ## Pre-Buffer Array for ADC to insure push button has been caught correctly
# prebuffy = array.array('H', (0 for index in range(20)))

## Time Data Array for ADC collection
time_data = array.array('H', (int(i*(ranger/freq)*1e3) for i in range(ranger)))

## Push Button Pin
Button = pyb.Pin(pyb.Pin.board.PC0, mode=pyb.Pin.IN)

## ADC On Button Pin
adc = pyb.ADC(Button)

## Timer Running at 200000 Hz
tim6 = pyb.Timer(6, freq = 200000)

## Boolean Flag indicating whether interrupt has succesfully run
boolflag = None
 
#Interrupt Function
def pushed(Line):
    ''' @brief   This interrupt is lets the code now to record the voltage 
                 from ADC from the blue button on the Nucleo
        @details IT is triggered on the button pushed release and flags the 
                 code to handle the data accorddingly
    '''
    print('button pushed')
    global boolflag
    global char
    if char == 'g':
        boolflag = 1
        
## Initialize Interrupt on Falling Edge of User Button
extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback=pushed)
           
while True:
    ## if the UI has sent the 'g' decode the variable accordingly
    if uart.any() != 0:
        print('decodeing ascii variable')
        char = uart.read().decode('ascii')
    ## If the button interrupt has been triggered clear variables and move on to the rest of the code
    elif boolflag == 1:
        print('button press registered')
        boolflag = 0
        char = None
        break
  
# while True:
#     #Pre-read the ADC and insure that the signal will be caught
#     adc.read_timed(prebuffy, tim6)
#     if prebuffy[-1] >= 10:
#         break

#True read the ADC and send array across serial to PCUI
adc.read_timed(buffy, tim6)
uart.write(str(buffy) + '  ' + str(time_data)+ '\r\n')
# while True:
#     for data in buffy:
#         uart.write('{:}, {:}\r\n'.format(tim6,data)

