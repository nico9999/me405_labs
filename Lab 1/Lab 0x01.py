''' @file    Lab 0x01.py 
    @brief   Documentation for use of Lab 0x01.py. Simulates a Vending machine
    @details A nine state FSM is impleted to simulate a Vending machine. This 
             code includes the getChange function, and how to handle key 
             inputs (0-7, C, S, D, P, E). 
    @author  Nicholas Holman
    @date    April. 15, 2020
'''

from time import sleep
import keyboard

def kb_cb(key):
    ''' @brief Callback function which is called when a key has been pressed.
    '''
    global last_key
    last_key = key.name
    
def getChange(price, payment):
    ''' @brief function that gets change from a purchase
        @param price  an integer input of how much a drink costs
        @param payment   a array of pennies to twenties of money for payment
        @return chng    an array of change computed from price and payment
    '''
    a = 0
    b = 0
    c = 0
    d = 0
    e = 0
    f = 0
    g = 0
    h = 0
    twenties = payment[7]*2000     # number of twenties in payment
    tens = payment[6]*1000         # number of tens in payment
    fives = payment[5]*500         # number of fives in payment
    ones = payment[4]*100          # number of ones in payment
    quarters = payment[3]*25       # number of quarters in payment
    dimes = payment[2]*10          # number of dimes in payment
    nickles = payment[1]*5         # number of nickles in payment
    pennies = payment[0]           # number of pennies in payment
    total = twenties + tens + fives + ones + quarters + dimes + nickles + pennies
    # total takes the list and converts it into a totla integer value
    change = total - price   # calculates the change due from price to payment 
    if change < 0:
        return ['none']
    else:
        while change >= 2000:          # the number of twenties due in change 
            change = change - 2000
            h = h + 1
        while change >= 1000:          # the number of tens due in change 
            change = change - 1000
            g = g + 1
        while change >= 500:           # the number of fives due in change 
            change = change - 500
            f = f + 1
        while change >= 100:           # the number of ones due in change 
            change = change - 100
            e = e + 1
        while change >= 25:            # the number of quarters due in change 
            change = change - 25
            d = d + 1
        while change >= 10:            # the number of dimes due in change 
            change = change - 10
            c = c + 1
        while change >= 5:             # the number of nickles due in change 
            change = change - 5
            b = b + 1
        while change > 0:             # the number of pennies due in change 
            change = change - 1
            a = a + 1
    chng = [a, b, c, d, e, f, g, h]  #takes each change value and puts it in a list for output
    return chng

def VendotronTask():
    """ @brief    simulates a vending machine for four drinks 
        @details  a 9 state FSM that takes coins and dollars as inputs. When 
                  the correct key is pressed for a soda, if funds are 
                  sufficient the soda is ejected and the total money left is 
                  displayed. A coin eject button is also an input
    """
    state = 8         # set to initialization state
    counter1 = 0
    pmt = [0, 0, 0, 0, 0, 0, 0, 0]
    cash = 0
    soda = False
    global last_key
    global change
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        # Code for key inputs here above the states 
        if last_key == 'c':
            # sets variables to handle a Cuke input
            soda = True
            Cuke = True
            last_key = ' '
            price = 100
        elif last_key == 'p':
            # sets variables to handle a Popsy input
            soda = True
            Popsy = True
            last_key = ' '
            price = 120
        elif last_key == 's':
            # sets variable to handle a Spryte input
            soda = True
            Spryte = True
            last_key = ' '
            price = 85
        elif last_key == 'd':
            # sets variables to handle a Dr. Pupper input
            soda = True
            DrP = True
            last_key = ' '
            price = 110
        elif last_key == '0':
            # sets variables to handle a penny input
            cash = 1
            last_key = ' '
            pmt[0] += 1
        elif last_key == '1':
            # sets variables to handle a nickle input
            cash = 1
            last_key = ' '
            pmt[1] += 1
        elif last_key == '2':
            # sets variables to handle a dime input
            cash = 1
            last_key = ' '
            pmt[2] += 1
        elif last_key == '3':
            # sets variables to handle a quarter input
            cash = 1
            last_key = ' '
            pmt[3] += 1
        elif last_key == '4':
            # sets variables to handle a one dollar input
            cash = 1
            last_key = ' '
            pmt[4] += 1
        elif last_key == '5':
            # sets variables to handle a five dollar input
            cash = 1
            last_key = ' '
            pmt[5] += 1
        elif last_key == '6':
            # sets variables to handle a ten dollar input
            cash = 1
            last_key = ' '
            pmt[6] += 1
        elif last_key == '7':
            # sets variables to handle a twenty dollar input
            cash = 1
            last_key = ' '
            pmt[7] += 1
        elif last_key == 'e':
            # sets varibale for eject
            state = 7
            last_key = ' '
        else:
            # if no buttons pressed do nothing 
            pass
        
        if state == 0:
            # perform state 0 operations 'init'
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            counter1 += 1   #increment a idle counter to dispcxs56cppsdppppppelay idle message 
            if (counter1 > 1000):
                #if idle in init state for too long switch to idle state 
                state = 1
                counter1 = 0       # reset counter1 
            elif (cash > 0):
                # switch to state to if a coin has been inserted
                state = 2   
                # cash = 0
            else:
                # remain in state 0 if neither exit condition has been met
                if soda == True: 
                    print('insert money before selecting a soda')
                    soda = False 
                    Cuke = False
                    Spryte = False
                    DrP = False
                    Popsy = False
                else:
                    pass
                state = 0
            
        elif state == 1:
            # perform state 1 operations 'idle'
            print('Try Sprote Today!')     # print idle message
            state = 0                      # return to state 0
            
        elif state == 2:
            # perform state 2 operations 'Display funds'
            if cash == 1:
                # calculate the total money inserted so far
                twenties = pmt[7]*2000     # number of twenties in payment
                tens = pmt[6]*1000         # number of tens in payment
                fives = pmt[5]*500         # number of fives in payment
                ones = pmt[4]*100          # number of ones in payment
                quarters = pmt[3]*25       # number of quarters in payment
                dimes = pmt[2]*10          # number of dimes in payment
                nickles = pmt[1]*5         # number of nickles in payment
                pennies = pmt[0]           # number of pennies in payment
                total = twenties + tens + fives + ones + quarters + dimes + nickles + pennies
                print(total)
                cash = 0
            else:
                pass
            if soda is True:
                state = 3
            else:
                pass

            
        elif state == 3:
            # perform state 3 operations 'soda button'
            # check if total funds is enough to buy the soda
            
            if (total >= price):
                state = 5
                print('state=5')
            else:
                state = 4
            
        elif state == 4:
            # perform state 4 operations 'insufficient funds'
            print('Insufficient funds, please insert more money, cost is:')
            soda = False 
            Cuke = False
            Spryte = False
            DrP = False
            Popsy = False
            cash = 0
            print(price)
            state = 2
            
        elif state == 5:
            # perform state 5 operations 'eject soda'
            # display proper message and compute change
            soda = False
            if Cuke == True:
                # message for Cuke
                print('Ejecting Cuke. Remaining Balance is:')
                pmt = getChange(price, pmt)
                print(pmt)
                Cuke = False
            elif Popsy == True:
                # message for Popsy
                print('Ejecting Popsy. Remaining Balance is:')
                pmt = getChange(price, pmt)
                print(pmt)  
                Popsy = False
            elif Spryte == True:
                # message for Spryte
                print('Ejecting Spryte. Remaining Balance is:')
                pmt = getChange(price, pmt)
                print(pmt)
                Spryte = False
            elif DrP == True:
                # message for Dr. Pupper
                print('Ejecting Dr. Pupper. Remaining Balance is:')
                pmt = getChange(price, pmt)
                print(pmt)
                DrP = False
            else:
                # reset various soda variables
                Cuke = False
                Popsy = False
                Spryte = False
                DrP = False
                pass
            
            twenties = pmt[7]*2000     # number of twenties in payment
            tens = pmt[6]*1000         # number of tens in payment
            fives = pmt[5]*500         # number of fives in payment
            ones = pmt[4]*100          # number of ones in payment
            quarters = pmt[3]*25       # number of quarters in payment
            dimes = pmt[2]*10          # number of dimes in payment
            nickles = pmt[1]*5         # number of nickles in payment
            pennies = pmt[0]           # number of pennies in payment
            total = twenties + tens + fives + ones + quarters + dimes + nickles + pennies
            if (total > 0):
                state = 6
            else:
                state = 0
                
        elif state == 6:
            # perform state 6 operations 'excess funds'
            print(total)   # debug
            print('Extra money detected. Select another soda')
            state = 2
        
        elif state == 7:
            # perform state 7 operations 'eject coins'
            print('Eject Detected, Collect coins below:')
            money = getChange(price, pmt)
            print(money)
            # set balance back to 0 
            pmt = [0, 0, 0, 0, 0, 0, 0, 0]
            state = 8
        
        elif state == 8:
            # I messed up the init state, so now state 8 is my init state and 
            # state 0 is the standby state
            Cuke = False
            Spryte = False
            Popsy = False
            DrP = False
            soda = False
            print('Vendotron machine active')
            state = 0
        
        else:    
            pass
        
        yield(state)



if __name__ == "__main__":
    # Initialize code to run FSM (not the init state). This means building the 
    # iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
   
    last_key = ''
    
    # Tell the keyboard module to respond to these particular keys only
    keyboard.on_release_key("c", callback=kb_cb)  # input for Cuke
    keyboard.on_release_key("p", callback=kb_cb)  # input for Popsi
    keyboard.on_release_key("s", callback=kb_cb)  # input for Spryte
    keyboard.on_release_key("d", callback=kb_cb)  # input for Dr. Pupper
    keyboard.on_release_key("e", callback=kb_cb)  # input to eject money
    keyboard.on_release_key("0", callback=kb_cb)  # input for pennies
    keyboard.on_release_key("1", callback=kb_cb)  # input for nickles 
    keyboard.on_release_key("2", callback=kb_cb)  # input for dimes
    keyboard.on_release_key("3", callback=kb_cb)  # input for quarters
    keyboard.on_release_key("4", callback=kb_cb)  # input for one dollar
    keyboard.on_release_key("5", callback=kb_cb)  # input for five dollar
    keyboard.on_release_key("6", callback=kb_cb)  # input for ten dollar
    keyboard.on_release_key("7", callback=kb_cb)  # input for twenty dollar
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')