''' @file    Lab0x04.py 
    @brief   This is the main code for Lab0x04 that will generate a CSV file from the data collected.
    @details 
    @author  Nicholas Holman
    @author  Vikram Thridandam
    @date    May 12, 2021
'''
import pyb
# import csv
from pyb import I2C, ADCAll
import utime
from mcp9808 import MCP9808

## Initialize the ADC
adc = pyb.ADCAll(12, 0x70000)

## Initialize CSV file
filename = 'Temperatures_Nucleo.csv'

## Initialize timing variable
ticks_0 = utime.ticks_ms()
t_iter = utime.ticks_ms()  # a second timing variable for interation in the while loop

## Initial time in integer for calculations in main loop
t0 = 0
cycle = 0

## Initialize the MCP9808 sensor Driver
MCP9808 = MCP9808(24)    #24 is the address
print('Driver initialized')

with open(filename, 'w') as csv:
    header = 'Time (s), Core Temperature (deg C), Ambient Temperature (deg C)\r\n'
    csv.write(header)
    while True:
        try:
            print('Reading temp cycle')
            print(str(cycle))
            
            #Read MCU VREF
            volts = adc.read_core_vref()
            
            #Read Core Temperature
            temp = adc.read_core_temp()
            print('Core Temp is: ' + str(temp))
            
            #Read ambient temperature from MCP9808
            temp_am = MCP9808.celsius()
            print('Temp from sensor is: ' + str(temp_am))
            
            #Get current ticks for timing
            t_current = utime.ticks_ms()
            
            #Calculate time from the start
            time_ms_run = utime.ticks_diff(t_current, t_iter)
            
            #Update Cycle time
            t_iter = t_current
            
            #Update total run time 
            t0 += time_ms_run            
            
            ## Iteration time in seconds
            time_s = t0/1000
            
            ## String that hold all the data recieved 
            data = str(time_s) + ', ' + str(temp) + ', ' + str(temp_am) + '\r\n'
            #Write data to csv file
            csv.write(data)
            print('Writing data to ' + filename + '. . . . . . . . . . . .')
            cycle += 1
            #sleep for one minute 
            utime.sleep(60)
        except KeyboardInterrupt:
            total_time = utime.ticks_diff(utime.ticks_ms(), ticks_0)
            hrs = total_time/3.6E6
            print('Data collected for ' + str(total_time/1000) + 'seconds, aka ' + str(hrs) + 'hours.')
            break
print('The .csv file is stored in the Nucleo: ' + filename + '.')
# with open(filename, 'w', newline='') as csvfile:
#     spamwriter = csv.writer(csv)
#     for n in range(len(data)):
#         spamwriter.writerow([time[n], data[n]])

