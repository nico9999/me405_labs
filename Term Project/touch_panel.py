'''@file touch_panel.py
   @brief This script holds the code to run the resitive touch panel
   @details This file does all the "work"?
   @author Nicholas Holman
   @author Vikram Thridandum
'''

import pyb
import utime
from touchdriver import touchdriver
import array

## Create the touch driver object
tch = touchdriver('PA0','PA6','PA1','PA7')

## Creat e an array to store the data from the driver
tch_buff = array.array('h',[])

## Create a csv file to store the data
with open ("touch_panel.csv","w") as csvfile:
    csvfile.write('Time [s], <insert what the other positions are here as well>')
    while True:
        try:
            # Line of code for testing 
            input('Press Enter to Start Data Collection')  
            while True:
                ## Get Timing for for the data collection
                t0 = utime.ticks_us()
                tch.read()
                t1 = utime.ticks_us()
                tdif = utime.ticks_diff(t1,t0)
                ## write the data from time and touch panel to csv
                line = str(tdif) + ',' + str(tch.pos[0]) + ',' + str(tch.pos[1]) + ',' + str(tch.pos[2])
                csvfile.write(line)
                
        except KeyboardInterrupt:
            print('Data Collection Finished \r\n')
            break
